package me.deftware.client.framework;

public class FrameworkConstants {

	public static double VERSION = 13.6;
	public static int PATCH = 1;
	public static boolean FORGE_MODE = false;

	public static String AUTHOR = "Deftware";
	public static String FRAMEWORK_NAME = "EMC";

}
