package me.deftware.mixin.mixins;

import me.deftware.client.framework.event.events.EventDisconnected;
import me.deftware.client.framework.event.events.EventGuiScreenDraw;
import net.minecraft.client.gui.GuiDisconnected;
import net.minecraft.client.gui.GuiScreen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(GuiDisconnected.class)
public class MixinGuiDisconnected {

	@Inject(method = "<init>*", at = @At("RETURN"))
	private void onConstructed(CallbackInfo ci) {
		new EventDisconnected().send();
	}

	@Inject(method = "render", at = @At("RETURN"))
	public void render(int mouseX, int mouseY, float partialTicks, CallbackInfo ci) {
		new EventGuiScreenDraw((GuiScreen) (Object) this).send();
	}

}
